package com.shloma.bbts.controller;

import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shloma.bbts.config.FXMLPage;
import com.shloma.bbts.config.ICONS;
import com.shloma.bbts.controller.support.AbstractSubPageController;
import com.shloma.bbts.tcp.TcpUtils;
import com.shloma.bbts.utils.BeanFactoryUtils;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * 主界面控制器
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/04
 * @version v1.0.0
 */
public class MainUIController extends AbstractSubPageController {

	private static Logger logger = LoggerFactory.getLogger(MainUIController.class);

	@FXML
	private Label startServer;
	@FXML
	private Label stopServer;
	@FXML
	private Label configManager;
	@FXML
	private Label logManager;
	@FXML
	private Label tradeInfo;
	@FXML
	private Label versionInfo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	public void initialize(URL location, ResourceBundle resources) {
		logger.info(">>>>>> 初始化主界面");
		setIcon(startServer, ICONS.START.getIconPath());
		setIcon(stopServer, ICONS.STOP.getIconPath());
		setIcon(configManager, ICONS.CONFIG.getIconPath());
		setIcon(logManager, ICONS.LOG.getIconPath());
		setIcon(tradeInfo, ICONS.TRADE.getIconPath());
		setIcon(versionInfo, ICONS.VERSION.getIconPath());
	}

	private void setIcon(Label bael, String iconUrl) {
		ImageView Image = new ImageView(iconUrl);
		Image.setFitHeight(25);
		Image.setFitWidth(25);
		bael.setGraphic(Image);
	}

	@FXML
	public void startServer() {
		// TcpServer server = BeanFactoryUtils.getBean(TcpServer.class);
		// server.startServer();
		TcpUtils server = BeanFactoryUtils.getBean(TcpUtils.class);
		server.startServer();

		startServer.setDisable(true);
		stopServer.setDisable(false);
	}

	@FXML
	public void stopServer() {
		// TcpServer server = BeanFactoryUtils.getBean(TcpServer.class);
		// server.stopServer();
		TcpUtils server = BeanFactoryUtils.getBean(TcpUtils.class);
		server.stopServer();

		startServer.setDisable(false);
		stopServer.setDisable(true);
	}

	@FXML
	public void showConfigManagerPage() {
		ConfigManagerController controller = (ConfigManagerController) loadFXMLPage(FXMLPage.CONFIG_MANAGER_PAGE,
				false);
		controller.setMainUIController(this);
		controller.setParentController(this);
		controller.showDialogStage();
	}

	@FXML
	public void showLogManagerPage() {
		LogManagerController controller = (LogManagerController) loadFXMLPage(FXMLPage.LOG_MANAGER_PAGE, false);
		controller.setMainUIController(this);
		controller.setParentController(this);
		controller.showDialogStage();
	}

	@FXML
	public void showTradeInfoPage() {
		TradeInfoController controller = (TradeInfoController) loadFXMLPage(FXMLPage.TRADE_INFO_PAGE, false);
		controller.setMainUIController(this);
		controller.setParentController(this);
		controller.showDialogStage();
	}

	@FXML
	public void showVersionInfoPage() {
		VersionInfoController controller = (VersionInfoController) loadFXMLPage(FXMLPage.VERSION_INFO_PAGE, false);
		controller.setMainUIController(this);
		controller.showDialogStage();
	}
}
