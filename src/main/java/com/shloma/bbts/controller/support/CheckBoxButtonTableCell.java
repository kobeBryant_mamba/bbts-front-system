/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shloma.bbts.controller.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

/**
 * 
 * Description: TODO(这里用一句话描述这个类的作用)
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/09
 * @version v1.0.0
 */
public class CheckBoxButtonTableCell<S, T> extends TableCell<S, T> {

	protected static Logger logger = LoggerFactory.getLogger(CheckBoxButtonTableCell.class);
	private final CheckBox chebox;

	public CheckBoxButtonTableCell() {
		this.chebox = new CheckBox();
		// 添加元素
		setGraphic(chebox);
	}

	@Override
	protected void updateItem(T item, boolean empty) {
		// logger.info("updateItem method empyty:{} item:{}", empty, item);
		super.updateItem(item, empty);
		if (empty) {
			// 如果此列为空默认不添加元素
			setText(null);
			setGraphic(null);
		} else {
			// 初始化为不选中
			chebox.setSelected(false);
			setGraphic(chebox);
		}
	}
}
