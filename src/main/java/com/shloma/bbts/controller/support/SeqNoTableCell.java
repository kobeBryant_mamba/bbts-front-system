package com.shloma.bbts.controller.support;

import javafx.scene.control.TableCell;

/**
 * Description: TODO(这里用一句话描述这个类的作用)
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/08
 * @version v1.0.0
 */
public class SeqNoTableCell<S, T> extends TableCell<S, T> {

	/* (non-Javadoc)
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	protected void updateItem(T item, boolean empty) {
		super.updateItem(item, empty);
		this.setText(null);
		this.setGraphic(null);

		if (!empty) {
			int rowIndex = this.getIndex() + 1;
			this.setText(String.valueOf(rowIndex));
		}
	}
}
