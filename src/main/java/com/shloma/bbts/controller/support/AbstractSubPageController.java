package com.shloma.bbts.controller.support;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shloma.bbts.App;
import com.shloma.bbts.config.FXMLPage;
import com.shloma.bbts.controller.MainUIController;
import com.shloma.bbts.utils.AlertUtil;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 弹出窗基类
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/07
 * @version v1.0.0
 */
public abstract class AbstractSubPageController implements Initializable {

	private static Logger logger = LoggerFactory.getLogger(AbstractSubPageController.class);
	private Stage primaryStage;
	private Stage dialogStage;

	private MainUIController mainUIController;
	private AbstractSubPageController parentController;

	private static Map<FXMLPage, SoftReference<? extends AbstractSubPageController>> cacheNodeMap = new HashMap<>();

	public AbstractSubPageController loadFXMLPage(FXMLPage fxmlPage, boolean cache) {
		SoftReference<? extends AbstractSubPageController> parentNodeRef = cacheNodeMap.get(fxmlPage);
		if (cache && parentNodeRef != null) {
			return parentNodeRef.get();
		}
		URL skeletonResource = Thread.currentThread().getContextClassLoader().getResource(fxmlPage.getFxml());
		FXMLLoader loader = new FXMLLoader(skeletonResource);
		Parent loginNode;
		try {
			loginNode = loader.load();
			AbstractSubPageController controller = loader.getController();
			dialogStage = new Stage();
			dialogStage.setTitle(fxmlPage.getPageTitle());
			// 必须关闭对话框后才能操作其他的
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			// 对话框永远在前面
			dialogStage.initOwner(getPrimaryStage());
			Scene scene = new Scene(loginNode);
			dialogStage.setScene(scene);
			dialogStage.setMaximized(false);
			dialogStage.setResizable(false);
			dialogStage.getIcons().add(new Image(App.class.getResourceAsStream(fxmlPage.getIcon())));
			dialogStage.show();
			controller.setDialogStage(dialogStage);
			// put into cache map
			SoftReference<AbstractSubPageController> softReference = new SoftReference<>(controller);
			cacheNodeMap.put(fxmlPage, softReference);

			return controller;
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			AlertUtil.showErrorAlert("程序异常", e.getMessage());
		}
		return null;
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public Stage getDialogStage() {
		return dialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void showDialogStage() {
		if (dialogStage != null) {
			dialogStage.show();
		}
	}

	public void closeDialogStage() {
		if (dialogStage != null) {
			dialogStage.close();
		}
	}

	/**
	 * @return the mainUIController
	 */
	public MainUIController getMainUIController() {
		return mainUIController;
	}

	/**
	 * @param mainUIController
	 *            the mainUIController to set
	 */
	public void setMainUIController(MainUIController mainUIController) {
		this.mainUIController = mainUIController;
	}

	/**
	 * @return the parentController
	 */
	public AbstractSubPageController getParentController() {
		return parentController;
	}

	/**
	 * @param parentController
	 *            the parentController to set
	 */
	public void setParentController(AbstractSubPageController parentController) {
		this.parentController = parentController;
	}
}
