/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shloma.bbts.controller.support;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;

/**
 *
 * @author Administrator
 */
public class RadioButtonTableCell<S, T> extends TableCell<S, T> {
    private final RadioButton radio;
    private ObservableValue<T> ov;

    public RadioButtonTableCell() {
        this.radio = new RadioButton();
        setAlignment(Pos.CENTER);
        setGraphic(radio);
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(radio);
            if (ov instanceof BooleanProperty) {
                radio.selectedProperty().unbindBidirectional(
                        (BooleanProperty) ov);
            }
            ov = getTableColumn().getCellObservableValue(getIndex());
            if (ov instanceof BooleanProperty) {
                radio.selectedProperty()
                        .bindBidirectional((BooleanProperty) ov);
            }
        }
    }
}
