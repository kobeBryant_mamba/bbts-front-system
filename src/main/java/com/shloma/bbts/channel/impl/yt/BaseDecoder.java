package com.shloma.bbts.channel.impl.yt;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.shloma.bbts.channel.Decoder;
import com.shloma.bbts.stream.yt.RootElement;

public abstract class BaseDecoder implements Decoder {

	public RootElement decode(String pkg, Class<?> elementType) {

		try {
			JAXBContext cxt = JAXBContext.newInstance(elementType);
			Unmarshaller unmarshaller = cxt.createUnmarshaller();
			RootElement root = (RootElement) unmarshaller.unmarshal(new StringReader(pkg));
			return root;
		} catch (JAXBException exception) {
			printError(exception);
			throw new RuntimeException(exception);
		}
	}

	public abstract void printError(Throwable throwable);
}
