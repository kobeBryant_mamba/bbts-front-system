package com.shloma.bbts.channel.impl.yt.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shloma.bbts.channel.impl.yt.BaseDecoder;

/**
 * 请求企业报文组装
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/09
 * @version v1.0.0
 */
public class ReceiveBankReponseDecoder extends BaseDecoder {

	private static Logger logger = LoggerFactory.getLogger(ReceiveBankReponseDecoder.class);

	@Override
	public void printError(Throwable throwable) {
		logger.error("######组装请求企业报文异常", throwable);
	}
}
