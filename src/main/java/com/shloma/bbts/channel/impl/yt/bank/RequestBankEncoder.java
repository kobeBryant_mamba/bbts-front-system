package com.shloma.bbts.channel.impl.yt.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shloma.bbts.channel.impl.yt.BaseEncoder;

/**
 * 请求企业报文组装
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/09
 * @version v1.0.0
 */
public class RequestBankEncoder extends BaseEncoder {

	private static Logger logger = LoggerFactory.getLogger(RequestBankEncoder.class);

	@Override
	public void printResult(byte[] pkgData) {
		logger.info("组装请求企业报文:{}", new String(pkgData));
	}

	@Override
	public void printError(Throwable throwable) {
		logger.error("######组装请求企业报文异常", throwable);
	}
}
