package com.shloma.bbts.channel.impl.yt;

import com.shloma.bbts.channel.impl.yt.bank.ReceiveBankReponseDecoder;
import com.shloma.bbts.channel.impl.yt.bank.RequestBankEncoder;
import com.shloma.bbts.stream.yt.bank.server.nest.BankBodyHead;
import com.shloma.bbts.stream.yt.bank.server.nest.pt001.RequestPT001;
import com.shloma.bbts.stream.yt.bank.server.nest.pt001.RequestPT001Root;

public class Test {

	public static void main(String[] args) {

		RequestBankEncoder bankEncoder = new RequestBankEncoder();
		
		RequestPT001Root pkg = new RequestPT001Root();
		BankBodyHead bankBodyHead = new BankBodyHead();
		bankBodyHead.setSeqNo("123123");
		pkg.setHead(bankBodyHead);
		
		RequestPT001 requestPT001 = new RequestPT001();
		requestPT001.setAcctNo("62222562");
		pkg.setBody(requestPT001);
		
		byte[] encode = bankEncoder.encode(pkg, RequestPT001Root.class);
		
		ReceiveBankReponseDecoder bankReponseDecoder = new ReceiveBankReponseDecoder();
		RequestPT001Root root = (RequestPT001Root)bankReponseDecoder.decode(new String(encode), RequestPT001Root.class);
		System.out.println(root.getHead().getSeqNo() + "==" + root.getBody().getAcctNo());
//		XmlRootElement root = (XmlRootElement)bankReponseDecoder.decode(new String(encode), RequestPT001Root.class);
//		System.out.println(root);
	}

}
