package com.shloma.bbts.channel;

import com.shloma.bbts.stream.yt.RootElement;

/**
 * 报文组装接口
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/09
 * @version v1.0.0
 */
public interface Encoder {

	public byte[] encode(RootElement pkg, Class<?> pkgType);
}
