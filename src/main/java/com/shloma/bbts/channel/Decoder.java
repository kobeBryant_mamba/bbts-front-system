package com.shloma.bbts.channel;

import com.shloma.bbts.stream.yt.RootElement;

/**
 * 报文解析接口
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/09
 * @version v1.0.0
 */
public interface Decoder {

	public RootElement decode(String pkg, Class<?> elementType);
}
