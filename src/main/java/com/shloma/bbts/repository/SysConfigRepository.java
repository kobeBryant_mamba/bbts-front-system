package com.shloma.bbts.repository;

import com.shloma.bbts.entity.SysConfig;

public interface SysConfigRepository {

	/**
	 * @return
	 */
	SysConfig getSysConfig();

	/**
	 * @param saveSysCOnfig
	 */
	void saveSysConfig(SysConfig saveSysCOnfig);


}
