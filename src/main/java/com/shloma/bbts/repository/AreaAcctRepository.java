package com.shloma.bbts.repository;

import java.util.List;

import com.shloma.bbts.entity.AreaAcct;

public interface AreaAcctRepository {

	/**
	 * @return
	 */
	List<AreaAcct> getAreaAccts();

	/**
	 * @param queryAreaAcct
	 * @return
	 */
	List<AreaAcct> getAreaAcctsByQueryCondition(AreaAcct queryAreaAcct);

	/**
	 * @param current
	 */
	void deleteSelectedItem(AreaAcct current);

	/**
	 * @param queryAreaAcct
	 */
	void addAreaAcct(AreaAcct queryAreaAcct);

	/**
	 * @param current
	 */
	void updateSelectedItem(AreaAcct current);

}
