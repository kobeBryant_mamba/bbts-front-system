package com.shloma.bbts.repository;

import com.shloma.bbts.entity.User;

public interface UserRepository {

	User getUser(User user);

}
