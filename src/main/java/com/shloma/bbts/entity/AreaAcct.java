package com.shloma.bbts.entity;

import javafx.beans.property.SimpleBooleanProperty;

/**
 * Description: TODO(这里用一句话描述这个类的作用)
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/08
 * @version v1.0.0
 */
public class AreaAcct {

	private String rgnCde;
	private String rgnNme;
	private String mainAcct;
	private String mainAcctNme;
	private SimpleBooleanProperty isCheck = new SimpleBooleanProperty();

	/**
	 * @return the rgnCde
	 */
	public String getRgnCde() {
		return rgnCde;
	}

	/**
	 * @param rgnCde
	 *            the rgnCde to set
	 */
	public void setRgnCde(String rgnCde) {
		this.rgnCde = rgnCde;
	}

	/**
	 * @return the rgnNme
	 */
	public String getRgnNme() {
		return rgnNme;
	}

	/**
	 * @param rgnNme
	 *            the rgnNme to set
	 */
	public void setRgnNme(String rgnNme) {
		this.rgnNme = rgnNme;
	}

	/**
	 * @return the mainAcct
	 */
	public String getMainAcct() {
		return mainAcct;
	}

	/**
	 * @param mainAcct
	 *            the mainAcct to set
	 */
	public void setMainAcct(String mainAcct) {
		this.mainAcct = mainAcct;
	}

	/**
	 * @return the mainAcctNme
	 */
	public String getMainAcctNme() {
		return mainAcctNme;
	}

	/**
	 * @param mainAcctNme
	 *            the mainAcctNme to set
	 */
	public void setMainAcctNme(String mainAcctNme) {
		this.mainAcctNme = mainAcctNme;
	}

	/**
	 * @return the isCheck
	 */
	public boolean getIsCheck() {
		return isCheck.get();
	}

	/**
	 * @param isCheck
	 *            the isCheck to set
	 */
	public void setIsCheck(boolean isCheck) {
		this.isCheck.set(isCheck);
	}
}
