package com.shloma.bbts.entity;

/**
 * 通讯参数配置
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/08
 * @version v1.0.0
 */
public class SysConfig {

	private String id;
	private int corpServerPort;
	private int bankServerPort;
	private String corpIp;
	private int corpPort;
	private String corpPkgEncode;
	private String bankIp;
	private int bankPort;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the corpServerPort
	 */
	public int getCorpServerPort() {
		return corpServerPort;
	}

	/**
	 * @param corpServerPort
	 *            the corpServerPort to set
	 */
	public void setCorpServerPort(int corpServerPort) {
		this.corpServerPort = corpServerPort;
	}

	/**
	 * @return the bankServerPort
	 */
	public int getBankServerPort() {
		return bankServerPort;
	}

	/**
	 * @param bankServerPort
	 *            the bankServerPort to set
	 */
	public void setBankServerPort(int bankServerPort) {
		this.bankServerPort = bankServerPort;
	}

	/**
	 * @return the corpIp
	 */
	public String getCorpIp() {
		return corpIp;
	}

	/**
	 * @param corpIp
	 *            the corpIp to set
	 */
	public void setCorpIp(String corpIp) {
		this.corpIp = corpIp;
	}

	/**
	 * @return the corpPort
	 */
	public int getCorpPort() {
		return corpPort;
	}

	/**
	 * @param corpPort
	 *            the corpPort to set
	 */
	public void setCorpPort(int corpPort) {
		this.corpPort = corpPort;
	}

	/**
	 * @return the corpPkgEncode
	 */
	public String getCorpPkgEncode() {
		return corpPkgEncode;
	}

	/**
	 * @param corpPkgEncode
	 *            the corpPkgEncode to set
	 */
	public void setCorpPkgEncode(String corpPkgEncode) {
		this.corpPkgEncode = corpPkgEncode;
	}

	/**
	 * @return the bankIp
	 */
	public String getBankIp() {
		return bankIp;
	}

	/**
	 * @param bankIp
	 *            the bankIp to set
	 */
	public void setBankIp(String bankIp) {
		this.bankIp = bankIp;
	}

	/**
	 * @return the bankPort
	 */
	public int getBankPort() {
		return bankPort;
	}

	/**
	 * @param bankPort
	 *            the bankPort to set
	 */
	public void setBankPort(int bankPort) {
		this.bankPort = bankPort;
	}
}
