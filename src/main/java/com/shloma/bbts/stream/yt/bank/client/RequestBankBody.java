package com.shloma.bbts.stream.yt.bank.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class RequestBankBody {

	@XmlElement(name = "data")
	private String data;
	@XmlElement(name = "rem")
	private String rem;
	@XmlElement(name = "systemId")
	private String systemId = "EBMP";

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

}
