package com.shloma.bbts.stream.yt.corp.server.acu006;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu006RequestBody {

	@XmlElement(name = "AcctNo")
	private String subAcct;

	/**
	 * @return the subAcct
	 */
	public String getSubAcct() {
		return subAcct;
	}

	/**
	 * @param subAcct
	 *            the subAcct to set
	 */
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

}
