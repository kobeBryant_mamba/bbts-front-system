package com.shloma.bbts.stream.yt.corp.server.acu001;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu001ResponseBody {

	@XmlElement(name = "AcctNo")
	private String subAcct;
	@XmlElement(name = "AcctName")
	private String acctName;
	@XmlElement(name = "OpenDate")
	private Date txnTme;
	@XmlElement(name = "RspCode")
	private String rspCode = "9999";

	/**
	 * @return the subAcct
	 */
	public String getSubAcct() {
		return subAcct;
	}

	/**
	 * @param subAcct
	 *            the subAcct to set
	 */
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

	/**
	 * @return the acctName
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * @param acctName
	 *            the acctName to set
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	/**
	 * @return the txnTme
	 */
	public Date getTxnTme() {
		return txnTme;
	}

	/**
	 * @param txnTme
	 *            the txnTme to set
	 */
	public void setTxnTme(Date txnTme) {
		this.txnTme = txnTme;
	}

	/**
	 * @return the rspCode
	 */
	public String getRspCode() {
		return rspCode;
	}

	/**
	 * @param rspCode
	 *            the rspCode to set
	 */
	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}

}
