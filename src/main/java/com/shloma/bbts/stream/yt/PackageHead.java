/**
 * 模块名称：淮北农民工工资代发
 * 开发公司：南京绿点信息科技有限公司
 */
package com.shloma.bbts.stream.yt;

import java.util.Date;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:53:39
 * @version 1.0.0
 */
public class PackageHead {

	private String thdTransCode;
	private String acuMsgType;
	private Date txnTme;
	private String txnSqn;

	public String getThdTransCode() {
		return thdTransCode;
	}

	public void setThdTransCode(String thdTransCode) {
		this.thdTransCode = thdTransCode;
	}

	public String getAcuMsgType() {
		return acuMsgType;
	}

	public void setAcuMsgType(String acuMsgType) {
		this.acuMsgType = acuMsgType;
	}

	public Date getTxnTme() {
		return txnTme;
	}

	public void setTxnTme(Date txnTme) {
		this.txnTme = txnTme;
	}

	public String getTxnSqn() {
		return txnSqn;
	}

	public void setTxnSqn(String txnSqn) {
		this.txnSqn = txnSqn;
	}

}
