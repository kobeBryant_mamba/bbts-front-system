package com.shloma.bbts.stream.yt.bank.client.nest.req3011;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Request3011 {
	@XmlElement(name = "IAcctNo")
	private String subAcct;
	@XmlElement(name = "StartDate")
	private Date startDate;
	@XmlElement(name = "EndDate")
	private Date endDate;

	@XmlElement(name = "ConNo")
	private String conNo;
	@XmlElement(name = "Bak1")
	private String bak1;

	public String getSubAcct() {
		return subAcct;
	}

	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getConNo() {
		return conNo;
	}

	public void setConNo(String conNo) {
		this.conNo = conNo;
	}

	public String getBak1() {
		return bak1;
	}

	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}

}
