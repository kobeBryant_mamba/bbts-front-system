package com.shloma.bbts.stream.yt.bank.client.nest.req3001;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Request3001 {

	@XmlElement(name = "IAcctNo")
	private String acctNo;
	@XmlElement(name = "HstSeqNo")
	private String hstSeqNo;
	@XmlElement(name = "InAcct")
	private String payNo;
	@XmlElement(name = "InName")
	private String PayName;
	@XmlElement(name = "InBankNo")
	private String payBankNo;
	@XmlElement(name = "InBankName")
	private String payBankName;
	@XmlElement(name = "InDate")
	private Date inDate;
	@XmlElement(name = "InTime")
	private String inTime;
	@XmlElement(name = "InAmount")
	private String payAmt;
	@XmlElement(name = "ExchangeRate")
	private String exchangeRate;
	@XmlElement(name = "TxnRmk")
	private String txnRmk;
	@XmlElement(name = "Bak1")
	private String bak1;

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getHstSeqNo() {
		return hstSeqNo;
	}

	public void setHstSeqNo(String hstSeqNo) {
		this.hstSeqNo = hstSeqNo;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getPayName() {
		return PayName;
	}

	public void setPayName(String payName) {
		PayName = payName;
	}

	public String getPayBankNo() {
		return payBankNo;
	}

	public void setPayBankNo(String payBankNo) {
		this.payBankNo = payBankNo;
	}

	public String getPayBankName() {
		return payBankName;
	}

	public void setPayBankName(String payBankName) {
		this.payBankName = payBankName;
	}

	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getTxnRmk() {
		return txnRmk;
	}

	public void setTxnRmk(String txnRmk) {
		this.txnRmk = txnRmk;
	}

	public String getBak1() {
		return bak1;
	}

	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}

}
