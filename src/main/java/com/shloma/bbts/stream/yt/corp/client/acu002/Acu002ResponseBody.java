package com.shloma.bbts.stream.yt.corp.client.acu002;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu002ResponseBody {

	@XmlElement(name = "RspCode")
	private String thdResult;

	/**
	 * @return the thdResult
	 */
	public String getThdResult() {
		return thdResult;
	}

	/**
	 * @param thdResult
	 *            the thdResult to set
	 */
	public void setThdResult(String thdResult) {
		this.thdResult = thdResult;
	}
}
