package com.shloma.bbts.stream.yt.corp.server.acu006;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 10:13:27
 * @version 1.0.0
 */
@XmlRootElement(name = "ebank")
@XmlAccessorType(XmlAccessType.FIELD)
public class Acu006ResponseRoot implements RootElement {
	@XmlElement(name = "body")
	private Acu006ResponseBody body;

	public Acu006ResponseBody getBody() {
		return body;
	}

	public void setBody(Acu006ResponseBody body) {
		this.body = body;
	}

}
