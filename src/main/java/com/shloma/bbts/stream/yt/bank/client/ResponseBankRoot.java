package com.shloma.bbts.stream.yt.bank.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

@XmlRootElement(name = "ap")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseBankRoot implements RootElement {
	@XmlElement(name = "head")
	private ResponseBankHead head;

	@XmlElement(name = "body")
	private ResponseBankBody body;

	public ResponseBankHead getHead() {
		return head;
	}

	public void setHead(ResponseBankHead head) {
		this.head = head;
	}

	public ResponseBankBody getBody() {
		return body;
	}

	public void setBody(ResponseBankBody body) {
		this.body = body;
	}

}
