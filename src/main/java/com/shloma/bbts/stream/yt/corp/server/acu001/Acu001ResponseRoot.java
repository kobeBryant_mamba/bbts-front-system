package com.shloma.bbts.stream.yt.corp.server.acu001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 10:13:27
 * @version 1.0.0
 */
@XmlRootElement(name = "ebank")
@XmlAccessorType(XmlAccessType.FIELD)
public class Acu001ResponseRoot implements RootElement {
	@XmlElement(name = "body")
	private Acu001ResponseBody body;

	public Acu001ResponseBody getBody() {
		return body;
	}

	public void setBody(Acu001ResponseBody body) {
		this.body = body;
	}

}
