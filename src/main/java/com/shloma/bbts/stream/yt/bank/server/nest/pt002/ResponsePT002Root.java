package com.shloma.bbts.stream.yt.bank.server.nest.pt002;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.server.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponsePT002Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private ResponsePT002 body;

	public ResponsePT002 getBody() {
		return body;
	}

	public void setBody(ResponsePT002 body) {
		this.body = body;
	}

}
