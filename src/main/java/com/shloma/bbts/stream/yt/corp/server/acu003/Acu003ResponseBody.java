package com.shloma.bbts.stream.yt.corp.server.acu003;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu003ResponseBody {

	@XmlElement(name = "AcctNo")
	private String subAcct;
	@XmlElement(name = "PayNo")
	private String oppAcct;
	@XmlElement(name = "PayName")
	private String oppName;
	@XmlElement(name = "PayTime")
	private Date payTime;
	@XmlElement(name = "PayAmt")
	private String txnAmt;
	@XmlElement(name = "ExchangeRate")
	private String exchangeRate;
	@XmlElement(name = "RspCode")
	private String rspCode = "9999";

	/**
	 * @return the subAcct
	 */
	public String getSubAcct() {
		return subAcct;
	}

	/**
	 * @param subAcct
	 *            the subAcct to set
	 */
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

	/**
	 * @return the oppAcct
	 */
	public String getOppAcct() {
		return oppAcct;
	}

	/**
	 * @param oppAcct
	 *            the oppAcct to set
	 */
	public void setOppAcct(String oppAcct) {
		this.oppAcct = oppAcct;
	}

	/**
	 * @return the oppName
	 */
	public String getOppName() {
		return oppName;
	}

	/**
	 * @param oppName
	 *            the oppName to set
	 */
	public void setOppName(String oppName) {
		this.oppName = oppName;
	}

	/**
	 * @return the payTime
	 */
	public Date getPayTime() {
		return payTime;
	}

	/**
	 * @param payTime
	 *            the payTime to set
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	/**
	 * @return the txnAmt
	 */
	public String getTxnAmt() {
		return txnAmt;
	}

	/**
	 * @param txnAmt
	 *            the txnAmt to set
	 */
	public void setTxnAmt(String txnAmt) {
		this.txnAmt = txnAmt;
	}

	/**
	 * @return the exchangeRate
	 */
	public String getExchangeRate() {
		return exchangeRate;
	}

	/**
	 * @param exchangeRate
	 *            the exchangeRate to set
	 */
	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * @return the rspCode
	 */
	public String getRspCode() {
		return rspCode;
	}

	/**
	 * @param rspCode
	 *            the rspCode to set
	 */
	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}

}
