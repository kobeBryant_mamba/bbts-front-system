package com.shloma.bbts.stream.yt.bank.client.nest.req3001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.client.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reponse3001Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private Reponse3001 body;

	public Reponse3001 getBody() {
		return body;
	}

	public void setBody(Reponse3001 body) {
		this.body = body;
	}

}
