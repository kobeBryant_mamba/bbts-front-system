package com.shloma.bbts.stream.yt.bank.server.nest;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class BankBodyHead {
	@XmlElement(name = "TransCode")
	private String transCode;

	@XmlElement(name = "TransDate")
	private Date txnDte;

	@XmlElement(name = "TransTime")
	private Date txnTme;

	@XmlElement(name = "SeqNo")
	private String seqNo;

	@XmlElement(name = "ZoneName")
	private String unitNo;

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public Date getTxnDte() {
		return txnDte;
	}

	public void setTxnDte(Date txnDte) {
		this.txnDte = txnDte;
	}

	public Date getTxnTme() {
		return txnTme;
	}

	public void setTxnTme(Date txnTme) {
		this.txnTme = txnTme;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getUnitNo() {
		return unitNo;
	}

	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}

}
