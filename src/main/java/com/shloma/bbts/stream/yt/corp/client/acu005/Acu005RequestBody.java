package com.shloma.bbts.stream.yt.corp.client.acu005;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu005RequestBody {

	@XmlElement(name = "AcctNo")
	private String acctNo;
	@XmlElement(name = "PayAmt")
	private String payAmt;
	@XmlElement(name = "ExchangeRate")
	private String exchangeRate;

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

}
