package com.shloma.bbts.stream.yt.corp.server.acu006;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 10:13:27
 * @version 1.0.0
 */
@XmlRootElement(name = "ebank")
@XmlAccessorType(XmlAccessType.FIELD)
public class Acu006RequestRoot implements RootElement {
	@XmlElement(name = "body")
	private Acu006RequestBody body;

	public Acu006RequestBody getBody() {
		return body;
	}

	public void setBody(Acu006RequestBody body) {
		this.body = body;
	}

}
