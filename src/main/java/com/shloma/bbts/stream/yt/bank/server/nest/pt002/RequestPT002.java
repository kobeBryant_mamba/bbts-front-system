package com.shloma.bbts.stream.yt.bank.server.nest.pt002;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class RequestPT002 {
	@XmlElement(name = "IAcctNo")
	private String acctNo;
	@XmlElement(name = "HstSeqNo")
	private String hstSeqNo;
	@XmlElement(name = "InAcct")
	private String payAcct;
	@XmlElement(name = "InName")
	private String payName;
	@XmlElement(name = "InBankNo")
	private String payBankNo;
	@XmlElement(name = "InBankName")
	private String payBankName;
	@XmlElement(name = "InDate")
	private Date txnDate;
	@XmlElement(name = "InTime")
	private Date txnTime;
	@XmlElement(name = "InAmount")
	private String payAmt;
	@XmlElement(name = "ExchangeRate")
	private String exchangeRate;
	@XmlElement(name = "TxnRmk")
	private String txnRmk;
	@XmlElement(name = "Bak1")
	private String bak1;

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getHstSeqNo() {
		return hstSeqNo;
	}

	public void setHstSeqNo(String hstSeqNo) {
		this.hstSeqNo = hstSeqNo;
	}

	public String getPayAcct() {
		return payAcct;
	}

	public void setPayAcct(String payAcct) {
		this.payAcct = payAcct;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public String getPayBankNo() {
		return payBankNo;
	}

	public void setPayBankNo(String payBankNo) {
		this.payBankNo = payBankNo;
	}

	public String getPayBankName() {
		return payBankName;
	}

	public void setPayBankName(String payBankName) {
		this.payBankName = payBankName;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public Date getTxnTime() {
		return txnTime;
	}

	public void setTxnTime(Date txnTime) {
		this.txnTime = txnTime;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getTxnRmk() {
		return txnRmk;
	}

	public void setTxnRmk(String txnRmk) {
		this.txnRmk = txnRmk;
	}

	public String getBak1() {
		return bak1;
	}

	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}

}
