package com.shloma.bbts.stream.yt.bank.client.nest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.shloma.bbts.stream.yt.RootElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class BankBodyRoot implements RootElement {
	@XmlElement(name = "head")
	protected BankBodyHead head;

	public BankBodyHead getHead() {
		return head;
	}

	public void setHead(BankBodyHead head) {
		this.head = head;
	}
}
