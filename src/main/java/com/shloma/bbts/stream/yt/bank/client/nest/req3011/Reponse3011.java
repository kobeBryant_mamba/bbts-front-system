package com.shloma.bbts.stream.yt.bank.client.nest.req3011;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Reponse3011 {

	@XmlElement(name = "Result")
	private String bankResult;
	@XmlElement(name = "AddWord")
	private String addWord;
	@XmlElement(name = "IAcctNo")
	private String subAcct;
	@XmlElement(name = "TotalNum")
	private String totalCount;
	@XmlElement(name = "Num")
	private String pageCount;
	@XmlElement(name = "IsCon")
	private String isCon;
	@XmlElement(name = "ConNo")
	private String conNo;
	@XmlElement(name = "Bak1")
	private String bak1;

	public String getBankResult() {
		return bankResult;
	}

	public void setBankResult(String bankResult) {
		this.bankResult = bankResult;
	}

	public String getAddWord() {
		return addWord;
	}

	public void setAddWord(String addWord) {
		this.addWord = addWord;
	}

	public String getSubAcct() {
		return subAcct;
	}

	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}

	public String getBak1() {
		return bak1;
	}

	public void setBak1(String bak1) {
		this.bak1 = bak1;
	}

}
