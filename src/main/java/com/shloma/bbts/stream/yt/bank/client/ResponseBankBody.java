package com.shloma.bbts.stream.yt.bank.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseBankBody {

	@XmlElement(name = "RspMsgContent")
	private String rspMsgContent;

	public String getRspMsgContent() {
		return rspMsgContent;
	}

	public void setRspMsgContent(String rspMsgContent) {
		this.rspMsgContent = rspMsgContent;
	}

}
