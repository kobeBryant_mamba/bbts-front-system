package com.shloma.bbts.stream.yt.bank.client.nest.req3011;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.client.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request3011Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private Request3011 body;

	public Request3011 getBody() {
		return body;
	}

	public void setBody(Request3011 body) {
		this.body = body;
	}

}
