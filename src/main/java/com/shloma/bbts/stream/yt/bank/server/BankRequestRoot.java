package com.shloma.bbts.stream.yt.bank.server;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

@XmlRootElement(name = "ap")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankRequestRoot implements RootElement {
	@XmlElement(name = "head")
	private BankRequestHead head;

	@XmlElement(name = "body")
	private BankRequestBody body;

	public BankRequestHead getHead() {
		return head;
	}

	public void setHead(BankRequestHead head) {
		this.head = head;
	}

	public BankRequestBody getBody() {
		return body;
	}

	public void setBody(BankRequestBody body) {
		this.body = body;
	}

}
