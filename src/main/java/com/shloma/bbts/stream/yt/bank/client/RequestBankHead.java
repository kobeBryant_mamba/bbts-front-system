package com.shloma.bbts.stream.yt.bank.client;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class RequestBankHead {

	@XmlElement(name = "tr_code")
	private String trCode;
	@XmlElement(name = "corp_no")
	private String corpNo;
	@XmlElement(name = "user_no")
	private String userNo;
	@XmlElement(name = "req_no")
	private String txnSqn;
	@XmlElement(name = "tr_acdt")
	private Date txnDte;
	@XmlElement(name = "tr_time")
	private Date txnTme;

	@XmlElement(name = "atom_tr_count")
	private String atomTrCount;
	@XmlElement(name = "channel")
	private String channel;
	@XmlElement(name = "reserved")
	private String reserved;

	public String getTrCode() {
		return trCode;
	}

	public void setTrCode(String trCode) {
		this.trCode = trCode;
	}

	public String getCorpNo() {
		return corpNo;
	}

	public void setCorpNo(String corpNo) {
		this.corpNo = corpNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getTxnSqn() {
		return txnSqn;
	}

	public void setTxnSqn(String txnSqn) {
		this.txnSqn = txnSqn;
	}

	public Date getTxnDte() {
		return txnDte;
	}

	public void setTxnDte(Date txnDte) {
		this.txnDte = txnDte;
	}

	public Date getTxnTme() {
		return txnTme;
	}

	public void setTxnTme(Date txnTme) {
		this.txnTme = txnTme;
	}

	public String getAtomTrCount() {
		return atomTrCount;
	}

	public void setAtomTrCount(String atomTrCount) {
		this.atomTrCount = atomTrCount;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

}
