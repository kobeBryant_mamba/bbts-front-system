package com.shloma.bbts.stream.yt.bank.server.nest.pt001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.server.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestPT001Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private RequestPT001 body;

	public RequestPT001 getBody() {
		return body;
	}

	public void setBody(RequestPT001 body) {
		this.body = body;
	}

}
