package com.shloma.bbts.stream.yt.bank.server.nest.pt002;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.server.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestPT002Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private RequestPT002 body;

	public RequestPT002 getBody() {
		return body;
	}

	public void setBody(RequestPT002 body) {
		this.body = body;
	}

}
