package com.shloma.bbts.stream.yt.bank.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseBankHead {
	@XmlElement(name = "tr_code")
	private String trCode;
	@XmlElement(name = "corp_no")
	private String corpNo;
	@XmlElement(name = "req_no")
	private String reqNo;
	@XmlElement(name = "serial_no")
	private String serialNo;
	@XmlElement(name = "ans_no")
	private String ansNo;

	@XmlElement(name = "next_no")
	private String nextNo;
	@XmlElement(name = "tr_acdt")
	private String trAcdt;
	@XmlElement(name = "tr_time")
	private String trTime;
	@XmlElement(name = "ans_code")
	private String ansCode;
	@XmlElement(name = "ans_info")
	private String ansInfo;

	@XmlElement(name = "particular_code")
	private String particularCode;
	@XmlElement(name = "particular_info")
	private String particularInfo;
	@XmlElement(name = "atom_tr_count")
	private String atomTrCount;
	@XmlElement(name = "reserved")
	private String reserved;

	public String getTrCode() {
		return trCode;
	}

	public void setTrCode(String trCode) {
		this.trCode = trCode;
	}

	public String getCorpNo() {
		return corpNo;
	}

	public void setCorpNo(String corpNo) {
		this.corpNo = corpNo;
	}

	public String getReqNo() {
		return reqNo;
	}

	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getAnsNo() {
		return ansNo;
	}

	public void setAnsNo(String ansNo) {
		this.ansNo = ansNo;
	}

	public String getNextNo() {
		return nextNo;
	}

	public void setNextNo(String nextNo) {
		this.nextNo = nextNo;
	}

	public String getTrAcdt() {
		return trAcdt;
	}

	public void setTrAcdt(String trAcdt) {
		this.trAcdt = trAcdt;
	}

	public String getTrTime() {
		return trTime;
	}

	public void setTrTime(String trTime) {
		this.trTime = trTime;
	}

	public String getAnsCode() {
		return ansCode;
	}

	public void setAnsCode(String ansCode) {
		this.ansCode = ansCode;
	}

	public String getAnsInfo() {
		return ansInfo;
	}

	public void setAnsInfo(String ansInfo) {
		this.ansInfo = ansInfo;
	}

	public String getParticularCode() {
		return particularCode;
	}

	public void setParticularCode(String particularCode) {
		this.particularCode = particularCode;
	}

	public String getParticularInfo() {
		return particularInfo;
	}

	public void setParticularInfo(String particularInfo) {
		this.particularInfo = particularInfo;
	}

	public String getAtomTrCount() {
		return atomTrCount;
	}

	public void setAtomTrCount(String atomTrCount) {
		this.atomTrCount = atomTrCount;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

}
