package com.shloma.bbts.stream.yt.corp.server.acu001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu001RequestBody {

	@XmlElement(name = "ApplyNo")
	private String applyNo;
	@XmlElement(name = "BatchNo")
	private String batchNo;
	@XmlElement(name = "UnitNo")
	private String unitNo;
	@XmlElement(name = "PayCurrency")
	private String ccy;

	/**
	 * @return the applyNo
	 */
	public String getApplyNo() {
		return applyNo;
	}

	/**
	 * @param applyNo
	 *            the applyNo to set
	 */
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	/**
	 * @return the batchNo
	 */
	public String getBatchNo() {
		return batchNo;
	}

	/**
	 * @param batchNo
	 *            the batchNo to set
	 */
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	/**
	 * @return the unitNo
	 */
	public String getUnitNo() {
		return unitNo;
	}

	/**
	 * @param unitNo
	 *            the unitNo to set
	 */
	public void setUnitNo(String unitNo) {
		this.unitNo = unitNo;
	}

	/**
	 * @return the ccy
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * @param ccy
	 *            the ccy to set
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

}
