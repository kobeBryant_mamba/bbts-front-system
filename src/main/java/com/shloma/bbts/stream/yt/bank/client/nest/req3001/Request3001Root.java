package com.shloma.bbts.stream.yt.bank.client.nest.req3001;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.bank.client.nest.BankBodyRoot;

@XmlRootElement(name = "root")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request3001Root extends BankBodyRoot {

	@XmlElement(name = "body")
	private Request3001 body;

	public Request3001 getBody() {
		return body;
	}

	public void setBody(Request3001 body) {
		this.body = body;
	}

}
