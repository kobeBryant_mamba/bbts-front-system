package com.shloma.bbts.stream.yt.corp.client.acu002;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * TODO
 * 
 * @author ld.sxf
 * @date 2018 12 10 12:56:38
 * @version 1.0.0
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Acu002RequestBody {

	@XmlElement(name = "AcctNo")
	private String acctNo;
	@XmlElement(name = "PayNo")
	private String payNo;
	@XmlElement(name = "PayName")
	private String PayName;
	@XmlElement(name = "PayTime")
	private Date payTime;
	@XmlElement(name = "PayAmt")
	private String payAmt;
	@XmlElement(name = "ExchangeRate")
	private String exchangeRate;

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getPayName() {
		return PayName;
	}

	public void setPayName(String payName) {
		PayName = payName;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

}
