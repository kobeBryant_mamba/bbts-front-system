package com.shloma.bbts.stream.yt.bank.server.nest.pt002;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponsePT002 {

	@XmlElement(name = "Result")
	private String rspCode = "EBMPMG9999";
	@XmlElement(name = "AddWork")
	private String addWork;

	public String getRspCode() {
		return rspCode;
	}

	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}

	public String getAddWork() {
		return addWork;
	}

	public void setAddWork(String addWork) {
		this.addWork = addWork;
	}

}
