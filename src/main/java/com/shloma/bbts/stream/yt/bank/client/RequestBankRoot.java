package com.shloma.bbts.stream.yt.bank.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.shloma.bbts.stream.yt.RootElement;

@XmlRootElement(name = "ap")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestBankRoot implements RootElement {
	@XmlElement(name = "head")
	private RequestBankHead head;

	@XmlElement(name = "body")
	private RequestBankBody body;

	public RequestBankHead getHead() {
		return head;
	}

	public void setHead(RequestBankHead head) {
		this.head = head;
	}

	public RequestBankBody getBody() {
		return body;
	}

	public void setBody(RequestBankBody body) {
		this.body = body;
	}

}
