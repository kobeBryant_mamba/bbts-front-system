package com.shloma.bbts;

import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shloma.bbts.config.FXMLPage;
import com.shloma.bbts.config.ICONS;
import com.shloma.bbts.utils.BeanFactoryUtils;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * 
 * 交易入口
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/04
 * @version v1.0.0
 */
public class App extends Application {

	private static Logger logger = LoggerFactory.getLogger(App.class);

	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {

		logger.info(">>>>> 初始化交易界面开始");
		URL url = Thread.currentThread().getContextClassLoader().getResource(FXMLPage.LOGIN_PAGE.getFxml());
		FXMLLoader fxmlLoader = new FXMLLoader(url);
		Parent root = fxmlLoader.load();
		primaryStage.setResizable(false);
		primaryStage.setTitle("烟台市招标通前置系统");
		primaryStage.setScene(new Scene(root));

		primaryStage.getIcons().add(new Image(App.class.getResourceAsStream(ICONS.LOGO.getIconPath())));
		primaryStage.show();
		logger.info(">>>>> 初始化交易界面结束");
	}

	public static void main(String[] args) {
		String version = System.getProperty("java.version");
		logger.info("current JDK version {}", version);
		if (Integer.parseInt(version.substring(2, 3)) >= 8 && Integer.parseInt(version.substring(6)) >= 60) {
			logger.info(">>>>> 初始化上下文服务开始");
			BeanFactoryUtils.loadApplication();
			logger.info(">>>>> 初始化上下文服务结束");

			launch(args);
		} else {
			JFrame jFrame = new JFrame("版本错误");
			jFrame.setSize(500, 100);
			jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JPanel jPanel = new JPanel();
			JLabel jLabel = new JLabel("JDK的版本不能低于1.8，请升级至最近的JDK 1.8再运行此软件");
			jPanel.add(jLabel);
			jFrame.add(jPanel);
			jFrame.setLocationRelativeTo(null);
			jFrame.setVisible(true);
		}
	}
}
