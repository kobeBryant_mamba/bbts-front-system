package com.shloma.bbts.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shloma.bbts.entity.User;
import com.shloma.bbts.repository.UserRepository;

@Service
public class UserService {

	private static Logger logger = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserRepository usersRepository;

	public boolean login(String name, String password) {

		User user = new User();
		user.setName(name);
		user.setPassword(password);
		logger.info(">>>>> 执行mybatis查询");
		User result = usersRepository.getUser(user);
		if (null != result) {
			return true;
		}
		return false;
	}
}
