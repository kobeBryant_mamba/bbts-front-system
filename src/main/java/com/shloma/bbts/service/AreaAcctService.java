package com.shloma.bbts.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shloma.bbts.entity.AreaAcct;
import com.shloma.bbts.repository.AreaAcctRepository;

/**
 * 系统配置服务类
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/08
 * @version v1.0.0
 */
@Service
public class AreaAcctService {
	private static Logger logger = LoggerFactory.getLogger(AreaAcctService.class);
	@Autowired
	private AreaAcctRepository acctRepository;

	/**
	 * @return
	 */
	public List<AreaAcct> getAreaAccts() {
		logger.info(">>>>> 查询行政区划列表");
		return acctRepository.getAreaAccts();
	}

	/**
	 * @param queryAreaAcct
	 * @return
	 */
	public List<AreaAcct> getAreaAcctsByQueryCondition(AreaAcct queryAreaAcct) {
		logger.info(">>>>> 查询行政区划列表");
		return acctRepository.getAreaAcctsByQueryCondition(queryAreaAcct);
	}

	/**
	 * @param current
	 */
	public void deleteSelectedItem(AreaAcct current) {
		acctRepository.deleteSelectedItem(current);
	}

	/**
	 * @param queryAreaAcct
	 */
	public void addAreaAcct(AreaAcct queryAreaAcct) {
		acctRepository.addAreaAcct(queryAreaAcct);
	}

	/**
	 * @param current
	 */
	public void updateSelectedItem(AreaAcct current) {
		acctRepository.updateSelectedItem(current);
	}
}
