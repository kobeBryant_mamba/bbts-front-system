package com.shloma.bbts.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shloma.bbts.entity.SysConfig;
import com.shloma.bbts.repository.SysConfigRepository;

/**
 * 系统配置服务类
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/08
 * @version v1.0.0
 */
@Service
public class SysConfigService {
	private static Logger logger = LoggerFactory.getLogger(SysConfigService.class);
	@Autowired
	private SysConfigRepository sysConfigRepository;

	public SysConfig querySysConfig() {
		logger.info(">>>>> 查询默认系统配置参数");
		return sysConfigRepository.getSysConfig();
	}

	/**
	 * @param saveSysCOnfig
	 */
	public void saveSysConfig(SysConfig saveSysCOnfig) {
		sysConfigRepository.saveSysConfig(saveSysCOnfig);
	}
}
