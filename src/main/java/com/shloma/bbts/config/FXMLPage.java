package com.shloma.bbts.config;

/**
 * 
 * 页面枚举
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/07
 * @version v1.0.0
 */
public enum FXMLPage {

	LOGIN_PAGE("登录", "view/Login.fxml", ICONS.LOGO), MAIN_UI_PAGE("主界面", "view/MainUI.fxml",
			ICONS.LOGO), CONFIG_MANAGER_PAGE("配置管理", "view/ConfigManager.fxml", ICONS.CONFIG), LOG_MANAGER_PAGE("日志管理",
					"view/LogManager.fxml", ICONS.LOG), TRADE_INFO_PAGE("交易信息", "view/TradeInfo.fxml",
							ICONS.TRADE), VERSION_INFO_PAGE("版本信息", "view/VersionInfo.fxml", ICONS.VERSION), ADD_AREA_ACCT_PAGE("新增行政区划主账号", "view/AddAreaAcct.fxml", ICONS.CONFIG);

	private String title;
	private String fxml;
	private ICONS icon;

	FXMLPage(String title, String fxml, ICONS icon) {
		this.title = title;
		this.fxml = fxml;
		this.icon = icon;
	}

	public String getPageTitle() {
		return this.title;
	}

	public String getFxml() {
		return this.fxml;
	}

	public String getIcon() {
		return this.icon.getIconPath();
	}
}
