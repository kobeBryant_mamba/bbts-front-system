package com.shloma.bbts.config;

/**
 * 图标类
 * <p>
 *
 * @author xuefeng.shi
 * @date 2018/12/07
 * @version v1.0.0
 */
public enum ICONS {

	LOGO("/icons/logo.png"), START("icons/start.png"), STOP("icons/stop.png"), CONFIG("/icons/config.png"), LOG(
			"/icons/log.png"), TRADE("/icons/trade.png"), VERSION("/icons/version.png");

	private String iconPath;

	ICONS(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getIconPath() {
		return this.iconPath;
	}
}
