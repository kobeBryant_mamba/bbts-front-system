package com.shloma.bbts.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanFactoryUtils {

	private static ApplicationContext APPLICATION;

	public static void loadApplication() {
		APPLICATION = new ClassPathXmlApplicationContext("classpath:config/spring-config.xml");
	}

	public static <T> T getBean(Class<T> beanType) {

		return APPLICATION.getBean(beanType);
	}
}
