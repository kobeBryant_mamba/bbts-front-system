package com.shloma.bbts.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionManager {

	private static Logger logger = LoggerFactory.getLogger(ConnectionManager.class);

	private static final String DB_URL = "jdbc:sqlite::resource:db/bbts.db";

	public static Connection getConnection() throws Exception {
		Class.forName("org.sqlite.JDBC");
		Connection conn = DriverManager.getConnection(DB_URL);
		return conn;
	}

	public static boolean login(String loginName, String loginPwd) {

		logger.info("登录开始");
		Connection conn = null;
		Statement stat = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getConnection();
			stat = conn.createStatement();
			String loginQuery = "select * from users where name = '" + loginName + "' and password = '" + loginPwd
					+ "'";
			rs = stat.executeQuery(loginQuery);
			if (rs.next()) {
				logger.info("登录成功");
				return true;
			}
		} catch (Exception e) {
			logger.error("登录异常", e);
		}
		logger.info("登录失败");
		return false;
	}
}
