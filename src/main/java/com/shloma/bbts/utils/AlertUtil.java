package com.shloma.bbts.utils;

import com.shloma.bbts.config.ICONS;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;

/**
 * Created by Owen on 6/21/16.
 */
public class AlertUtil {

	public static void showInfoAlert(String title, String message) {

		doShowAlert(title, message, Alert.AlertType.INFORMATION);
	}

	public static void showWarnAlert(String title, String message) {
		doShowAlert(title, message, Alert.AlertType.WARNING);
	}

	public static void showErrorAlert(String title, String message) {
		doShowAlert(title, message, Alert.AlertType.ERROR);
	}

	/**
	 * build both OK and Cancel buttons for the user to click on to dismiss the
	 * dialog.
	 *
	 * @param message
	 */
	public static Alert showConfirmationAlert(String title, String message) {

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, new ButtonType("取消", ButtonBar.ButtonData.NO),
				new ButtonType("确定", ButtonBar.ButtonData.YES));
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(message);
		return alert;
	}

	/**
	 * @param message
	 * @param alert
	 */
	private static void doShowAlert(String title, String message, Alert.AlertType alertType) {
		Alert alert = new Alert(alertType);
		alert.setHeaderText(null);
		alert.setTitle(title);
		alert.setContentText(message);

		ImageView Image = new ImageView(ICONS.VERSION.getIconPath());
		Image.setFitHeight(25);
		Image.setFitWidth(25);
		alert.setGraphic(Image);
		alert.show();
	}

}
